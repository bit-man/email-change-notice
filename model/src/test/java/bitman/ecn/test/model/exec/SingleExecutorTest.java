package bitman.ecn.test.model.exec;

import bitman.ecn.model.exec.EMailAlreadySendException;
import bitman.ecn.model.exec.SingleExecutor;
import bitman.ecn.model.input.InputRow;
import bitman.ecn.model.input.InputRowImpl;
import bitman.ecn.model.mail.MailSendErrorException;
import bitman.ecn.model.status.OperationResult;
import bitman.ecn.model.status.StatusIteratorImpl;
import bitman.ecn.test.Util;
import bitman.ecn.test.model.mail.MailSenderFailsMock;
import bitman.ecn.test.model.mail.MailSenderMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SingleExecutorTest
{

    private static final String EMAIL_SENT = "email1@sample.org";
    private static final String EMAIL_NOT_SENT = "email2@sample.org";
    private static final String EMAIL_SEND_FAILED = "email3@sample.org";
    private static final String EMAIL_FROM = "from@example.com";

    private static InputRow inputEmailSent;
    private static String statusStr;
    private static InputRow inputEmailNotSent;
    private static InputRow inputEmailSendFailed;
    private static String tempFile;

    @BeforeClass
    public static void setupBeforeClass()
            throws IOException
    {
        statusStr = EMAIL_SENT + "," + OperationResult.OK + "\n" +
                    EMAIL_SEND_FAILED + "," + OperationResult.FailedSend;

        tempFile = Util.createTempFile().getAbsolutePath();
        inputEmailSent = new InputRowImpl(EMAIL_SENT + "," + tempFile);
        inputEmailNotSent = new InputRowImpl(EMAIL_NOT_SENT + "," + tempFile);
        inputEmailSendFailed = new InputRowImpl(EMAIL_SEND_FAILED + "," + tempFile);
    }

    @Test
    public void testConstructor() {
        assertNotNull( new SingleExecutor(inputEmailSent, null, null, EMAIL_FROM) );
    }

    @Test(expected = EMailAlreadySendException.class)
    public void testMailSentOkIsNotResent()
            throws EMailAlreadySendException, MailSendErrorException, IOException
    {
        final BufferedReader status = new BufferedReader(new StringReader(statusStr));
        final SingleExecutor executor = new SingleExecutor(
                    inputEmailSent,
                    new StatusIteratorImpl(status),
                    new MailSenderFailsMock(),
                EMAIL_FROM);
        executor.send();
    }

    @Test
    public void testMailNotSentDoesntFailOnSend()
            throws EMailAlreadySendException, MailSendErrorException, IOException
    {
        final BufferedReader status = new BufferedReader(new StringReader(statusStr) );
        final MailSenderMock mailSender = new MailSenderMock();
        final SingleExecutor executor = new SingleExecutor(
                inputEmailNotSent,
                new StatusIteratorImpl(status),
                mailSender,
                EMAIL_FROM);
        executor.send();
        assertTrue(mailSender.isSendMessageInvoked());
    }


    @Test
    public void testMailFailedIsResent()
            throws EMailAlreadySendException, MailSendErrorException, IOException
    {
        final BufferedReader status = new BufferedReader(new StringReader(statusStr));
        final MailSenderMock mailSender = new MailSenderMock();
        final SingleExecutor executor = new SingleExecutor(
                inputEmailSendFailed,
                new StatusIteratorImpl(status),
                mailSender,
                EMAIL_FROM);
        executor.send();
        assertTrue(mailSender.isSendMessageInvoked());
    }

    @Test(expected = MailSendErrorException.class)
    public void testMailFailedFailsOnResent()
            throws EMailAlreadySendException, MailSendErrorException, IOException
    {
        final BufferedReader status = new BufferedReader(new StringReader(statusStr));
        final MailSenderMock mailSender = new MailSenderFailsMock();
        final SingleExecutor executor = new SingleExecutor(
                inputEmailSendFailed,
                new StatusIteratorImpl(status),
                mailSender,
                EMAIL_FROM);
        executor.send();
    }

    @Test(expected = MailSendErrorException.class)
    public void testMailNotSentFailsOnResent()
            throws EMailAlreadySendException, MailSendErrorException, IOException
    {
        final BufferedReader status = new BufferedReader(new StringReader(statusStr));
        final MailSenderMock mailSender = new MailSenderFailsMock();
        final SingleExecutor executor = new SingleExecutor(
                inputEmailNotSent,
                new StatusIteratorImpl(status),
                mailSender,
                EMAIL_FROM);
        executor.send();
    }

    @Test(expected = EMailAlreadySendException.class)
    public void testMailNotSentAndSentLaterIsNotResend()
            throws EMailAlreadySendException, MailSendErrorException, IOException
    {
        String statusStr2 = EMAIL_SENT + "," + OperationResult.FailedSend + "\n" +
                EMAIL_SEND_FAILED + "," + OperationResult.FailedSend  + "\n" +
                EMAIL_SENT + "," + OperationResult.OK;
        final BufferedReader status = new BufferedReader(new StringReader(statusStr2));
        final MailSenderMock mailSender = new MailSenderMock();
        final SingleExecutor executor = new SingleExecutor(
                inputEmailSent,
                new StatusIteratorImpl(status),
                mailSender,
                EMAIL_FROM);
        executor.send();
    }

    @Test
    public void testMailNotSentMoreThanOnceIsSend()
            throws EMailAlreadySendException, MailSendErrorException, IOException
    {
        String statusStr2 = EMAIL_SENT + "," + OperationResult.FailedSend + "\n" +
                EMAIL_SENT + "," + OperationResult.FailedSend;
        final BufferedReader status = new BufferedReader(new StringReader(statusStr2));
        final MailSenderMock mailSender = new MailSenderMock();
        final SingleExecutor executor = new SingleExecutor(
                inputEmailSent,
                new StatusIteratorImpl(status),
                mailSender,
                EMAIL_FROM);
        executor.send();

        assertTrue(mailSender.isSendMessageInvoked());
    }

    @Test
    public void test2MailAlreadySentUsingSameStatusIteratorBothAreNotSend()
            throws EMailAlreadySendException, MailSendErrorException, IOException
    {
        String statusStr2 = EMAIL_SEND_FAILED + "," + OperationResult.AlreadySent + "\n" +
                EMAIL_NOT_SENT + "," + OperationResult.AlreadySent;
        final BufferedReader status = new BufferedReader(new StringReader(statusStr2));
        final MailSenderMock mailSender = new MailSenderMock();

        // Send e-mail for EMAIL_NOT_SENT
        InputRow inputEmail = new InputRowImpl(EMAIL_NOT_SENT + "," + tempFile);
        StatusIteratorImpl statusIterator = new StatusIteratorImpl(status);
        SingleExecutor executor = new SingleExecutor(
                inputEmail,
                statusIterator,
                mailSender,
                EMAIL_FROM);
        try
        {
            executor.send();
            fail("E-mail " + EMAIL_NOT_SENT + " must not be send again");
        } catch (EMailAlreadySendException e)
        {
            // OK, test passed
        }

        mailSender.reset();

        // Send e-mail for EMAIL_SEND_FAILED
        inputEmail = new InputRowImpl(EMAIL_SEND_FAILED + "," + tempFile);
        executor = new SingleExecutor(
                inputEmail,
                statusIterator,
                mailSender,
                EMAIL_FROM);
        try
        {
            executor.send();
            fail("E-mail " + EMAIL_SEND_FAILED + " must not be send again");
        } catch (EMailAlreadySendException e)
        {
            // OK, test passed
        }


    }

}
