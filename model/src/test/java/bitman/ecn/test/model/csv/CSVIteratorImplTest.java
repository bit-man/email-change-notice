package bitman.ecn.test.model.csv;

import bitman.ecn.model.csv.CSVIteratorImpl;
import bitman.ecn.model.csv.CSVRowImpl;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.Assert.*;

public class CSVIteratorImplTest
{
    @Test
    public void testConstructor() {

        BufferedReader in = new BufferedReader( new StringReader("field1,field2"));
        assertNotNull(new CSVIteratorImpl(in));
    }

    @Test
    public void testNoRowsNoHasNext() {

        BufferedReader in = new BufferedReader( new StringReader(""));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        assertFalse(csvIterator.hasNext());
    }

    @Test
    public void testOneRowHasNext() {

        BufferedReader in = new BufferedReader( new StringReader("field1,field2"));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        assertTrue(csvIterator.hasNext());
    }

    @Test
    public void testTwoRowFirstNextHasNext() {

        BufferedReader in = new BufferedReader( new StringReader("field1a,field1b\nfield2a,field2b"));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        csvIterator.next();
        assertTrue(csvIterator.hasNext());
    }

    @Test
    public void testTwoRowHasSecondNextHasNoNext() {

        BufferedReader in = new BufferedReader( new StringReader("field1a,field1b\nfield2a,field2b"));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        csvIterator.next();
        csvIterator.next();
        assertFalse(csvIterator.hasNext());
    }

    @Test
    public void testTwoRowHasOnlyTwoNext() {

        BufferedReader in = new BufferedReader( new StringReader("field1a,field1b\n" +
                "field2a,field2b\n" +
                "field3a,field3b"));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        csvIterator.next();
        csvIterator.next();
        csvIterator.next();
        assertFalse(csvIterator.hasNext());
    }


    @Test
    public void testOnInitWithMultipleRowsFirstRowIsAvailable() {

        BufferedReader in = new BufferedReader( new StringReader("field1a,field1b\nfield2a,field2b"));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        assertEquals( new CSVRowImpl("field1a,field1b"), csvIterator.next() );
    }

    @Test
    public void testOneRowNext() {

        BufferedReader in = new BufferedReader( new StringReader("field1,field2"));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        assertEquals(new CSVRowImpl("field1,field2"), csvIterator.next());
    }

    @Test
    public void testTwoRowHasGetTwoNext() {

        BufferedReader in = new BufferedReader( new StringReader("field1a,field1b\nfield2a,field2b"));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        csvIterator.next();
        csvIterator.next();
        assertEquals(new CSVRowImpl("field2a,field2b"), csvIterator.next());
    }

    @Test
    public void testTwoRowHasGetThreeNext() {

        BufferedReader in = new BufferedReader( new StringReader("field1a,field1b\n" +
                "field2a,field2b\n" +
                "field3a,field3b"));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        csvIterator.next();
        csvIterator.next();
        csvIterator.next();
        assertEquals(new CSVRowImpl("field3a,field3b"), csvIterator.next() );

    }


    @Test( expected = IllegalArgumentException.class)
    public void testCantRemove() {

        BufferedReader in = new BufferedReader( new StringReader(""));
        final CSVIteratorImpl csvIterator = new CSVIteratorImpl(in);

        csvIterator.remove();
    }

}
