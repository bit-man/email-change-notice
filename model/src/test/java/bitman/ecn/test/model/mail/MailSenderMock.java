package bitman.ecn.test.model.mail;

import bitman.ecn.model.mail.MailSendErrorException;
import bitman.ecn.model.mail.SMTPAbstractMailSender;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;

public class MailSenderMock
        extends SMTPAbstractMailSender
{
    public static final String ERROR_MESSAGE = "Unknown reason";
    protected boolean sendMessageInvoked;

    public MailSenderMock()
    {
        this.sendMessageInvoked = false;
    }

    @Override
    public void setCredentials(String user, String password)
    {

    }

    @Override
    public void sendMessage(BufferedReader in, String from, @NotNull String to)
            throws MailSendErrorException
    {
        sendMessageInvoked = true;
     }


    public boolean isSendMessageInvoked()
    {
        return sendMessageInvoked;
    }

    public void reset() {
        sendMessageInvoked = false;
    }

}
