package bitman.ecn.test.model.csv;

import bitman.ecn.model.csv.CSVRow;
import bitman.ecn.model.csv.CSVRowImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CSVRowTest
{
    @Test
    public void testConstructor()
    {
        assertNotNull(new CSVRowImpl(""));
    }

    @Test
    public void testEmptyRowHasOneField()
    {
        final CSVRow csvRow = new CSVRowImpl("");
        assertEquals(1, csvRow.fields());
    }

    @Test
    public void testEmptyRowHasOneEmptyField()
    {
        final CSVRow csvRow = new CSVRowImpl("");
        assertEquals("", csvRow.getField(0));
    }

    @Test
    public void testRowHasOneFields()
    {
        final CSVRow csvRow = new CSVRowImpl("field1");
        assertEquals(1, csvRow.fields());
    }

    @Test
    public void testRowHasTwoFields()
    {
        final CSVRow csvRow = new CSVRowImpl("field1,field2");
        assertEquals(2, csvRow.fields());
    }

    @Test
    public void testRowGetSecondField()
    {
        final CSVRow csvRow = new CSVRowImpl("field1,field2");
        assertEquals("field2", csvRow.getField(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRowGetInvalidField()
    {
        final CSVRow csvRow = new CSVRowImpl("field1,field2");
        csvRow.getField(2);
    }

}
