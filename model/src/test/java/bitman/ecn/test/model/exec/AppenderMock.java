package bitman.ecn.test.model.exec;

import java.io.IOException;
import java.io.Writer;

public class AppenderMock
    extends Writer
{
    private String appendedText;

    public AppenderMock() {
        appendedText = "";
    }

    @Override
    @SuppressWarnings("all")
    public void write(char[] cbuf, int off, int len)
            throws IOException
    {
        appendedText = new String(cbuf);
    }


    @SuppressWarnings("all")
    public void write(String str) throws IOException {
        appendedText = str;
    }


    @Override
    public void flush()
            throws IOException
    {

    }

    @Override
    public void close()
            throws IOException
    {

    }

    public String getAppendedText()
    {
        return appendedText;
    }
}
