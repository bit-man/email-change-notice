package bitman.ecn.test.model.input;

import bitman.ecn.model.input.InputField;
import bitman.ecn.model.input.InputRow;
import bitman.ecn.model.input.InputRowImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class InputRowTest
{
    @Test
    public void testConstructor()
    {
        assertNotNull(new InputRowImpl(""));
    }

    @Test
    public void testEmptyRowHasOneField()
    {
        final InputRow csvRow = new InputRowImpl("");
        assertEquals(1, csvRow.fields());
    }

    @Test
    public void testEmptyRowHasOneEmptyField()
    {
        final InputRow csvRow = new InputRowImpl("");
        assertEquals("", csvRow.getField(0));
    }

    @Test
    public void testRowHasOneFields()
    {
        final InputRow csvRow = new InputRowImpl("field1");
        assertEquals(1, csvRow.fields());
    }

    @Test
    public void testRowHasTwoFields()
    {
        final InputRow csvRow = new InputRowImpl("field1,field2");
        assertEquals(2, csvRow.fields());
    }

    @Test
    public void testRowGetSecondField()
    {
        final InputRow csvRow = new InputRowImpl("field1,field2");
        assertEquals("field2", csvRow.getField(1));
    }

    @Test
    public void testRowGetEMAIL()
    {
        final InputRow csvRow = new InputRowImpl("field1,field2");
        assertEquals("field1", csvRow.getField(InputField.EMAIL));
    }

    @Test
    public void testRowGetMSGID()
    {
        final InputRow csvRow = new InputRowImpl("field1,field2");
        assertEquals("field2", csvRow.getField(InputField.MESSAGE_ID));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRowGetInvalidField()
    {
        final InputRow csvRow = new InputRowImpl("field1,field2");
        csvRow.getField(2);
    }

}
