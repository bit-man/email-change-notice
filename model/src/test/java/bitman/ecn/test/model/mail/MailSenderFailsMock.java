package bitman.ecn.test.model.mail;

import bitman.ecn.model.mail.MailSendErrorException;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;

public class MailSenderFailsMock
        extends MailSenderMock
{
    public static final String ERROR_MESSAGE = "Unknown reason";


    public MailSenderFailsMock()
    {
        super();
    }

    @Override
    public void setCredentials(String user, String password)
    {

    }

    @Override
    public void sendMessage(BufferedReader in, String from, @NotNull String to)
            throws MailSendErrorException
    {
        super.sendMessage(in, from, to);
        throw new MailSendErrorException(ERROR_MESSAGE);
    }

}
