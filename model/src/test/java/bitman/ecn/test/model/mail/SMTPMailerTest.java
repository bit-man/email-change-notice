package bitman.ecn.test.model.mail;

import bitman.ecn.model.mail.MailSendErrorException;
import bitman.ecn.model.mail.SMTPMailer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SMTPMailerTest
{

    private static final String EMAIL_FROM = "user@example.com";
    private static final String EMAIL_TO = "foo@bar.com";
    private static final String SUBJECT = "TestMail";

    @Test
    public void testConstructor() {
        assertNotNull( new SMTPMailer("host", 25, null) );
    }


    @Test( expected = MailSendErrorException.class)
    public void testSenderFailsAndThrowsException()
            throws MailSendErrorException
    {
        SMTPMailer smtpMailer = new SMTPMailer("host", 25, new MailSenderFailsMock());
        smtpMailer.sendMessage(null, EMAIL_FROM, EMAIL_TO, SUBJECT);
    }

    @Test
    public void testSenderFailsAndThrowsExceptionWithMessage()
    {
        SMTPMailer smtpMailer = new SMTPMailer("host", 25, new MailSenderFailsMock());
        try
        {
            smtpMailer.sendMessage(null, EMAIL_FROM, EMAIL_TO, SUBJECT);
        } catch (MailSendErrorException e)
        {
            assertEquals(MailSenderMock.ERROR_MESSAGE, e.getMessage());
        }
    }

    @Test
    public void testSenderNoFail()
            throws MailSendErrorException
    {
        SMTPMailer smtpMailer = new SMTPMailer("host", 25, new MailSenderMock());
        smtpMailer.sendMessage(null, EMAIL_FROM, EMAIL_TO, SUBJECT);
    }


}
