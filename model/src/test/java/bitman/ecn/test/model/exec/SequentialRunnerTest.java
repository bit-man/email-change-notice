package bitman.ecn.test.model.exec;

import bitman.ecn.model.exec.SequentialRunner;
import bitman.ecn.model.status.OperationResult;
import bitman.ecn.test.Util;
import bitman.ecn.test.model.mail.MailSenderFailsMock;
import bitman.ecn.test.model.mail.MailSenderMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SequentialRunnerTest
{
    private static final String EMAIL1 = "email1@example.org";
    private static final String EMAIL2 = "email2@example.org";
    private static final String FILE_DOES_NOT_EXISTS = "/does/not/exists";
    private static String inputStrEMail1;
    private static String statusStrEmail1OK;
    private static String statusStrEmail1FailedSend;
    private static String inputStrEMail1FileDoesntExist;
    private static String statusStrEmail1AlreadySend;

    @BeforeClass
    public static void setupBeforeClass()
            throws IOException
    {
        final File tempFile = Util.createTempFile();
        inputStrEMail1 = EMAIL1 + "," + tempFile.getAbsolutePath();
        inputStrEMail1FileDoesntExist = EMAIL1 + "," + FILE_DOES_NOT_EXISTS;
        statusStrEmail1OK = EMAIL1 + "," + OperationResult.OK + ",\n";
        statusStrEmail1FailedSend = EMAIL1 + "," + OperationResult.FailedSend + ",\n";
        statusStrEmail1AlreadySend = EMAIL1 + "," + OperationResult.AlreadySent + ",\n";
    }

    @Test
    public void testConstructorNotNull() {
        assertNotNull(new SequentialRunner(new RunnerRequestMock("", statusStrEmail1OK, new MailSenderMock())));
    }

    @Test
    public void testMailStatusOKAddedToStatusAsAlreadySent() {
        final RunnerRequestMock request = new RunnerRequestMock(inputStrEMail1, statusStrEmail1OK, new MailSenderMock());
        new SequentialRunner(request).run();

        final AppenderMock statusAppender = (AppenderMock) request.getStatusAppender();
        assertTrue(statusAppender.getAppendedText().contains(Util.asCSVfield(OperationResult.AlreadySent)));
    }

    @Test
    public void testMailStatusFailedSendAndFailedSendingAddsToStatusSentFailed() {
        final RunnerRequestMock request = new RunnerRequestMock(inputStrEMail1, statusStrEmail1FailedSend, new MailSenderFailsMock());
        new SequentialRunner(request).run();

        final AppenderMock statusAppender = (AppenderMock) request.getStatusAppender();
        assertTrue(statusAppender.getAppendedText().contains(Util.asCSVfield(OperationResult.FailedSend)));
    }

    @Test
    public void testMailStatusFailedSendAndSendingAddsToStatusSentOK() {
        final RunnerRequestMock request = new RunnerRequestMock(inputStrEMail1, statusStrEmail1FailedSend, new MailSenderMock());
        new SequentialRunner(request).run();

        final AppenderMock statusAppender = (AppenderMock) request.getStatusAppender();
        assertTrue(statusAppender.getAppendedText().contains(Util.asCSVfield(OperationResult.OK)));
    }

    @Test
    public void testMailMessageNotFoundSendAddsToStatusFileNotFound() {
        final RunnerRequestMock request = new RunnerRequestMock(inputStrEMail1FileDoesntExist, statusStrEmail1FailedSend, new MailSenderMock());
        new SequentialRunner(request).run();

        final AppenderMock statusAppender = (AppenderMock) request.getStatusAppender();
        assertTrue(statusAppender.getAppendedText().contains(Util.asCSVfield(OperationResult.FailedSend)));
        // ToDo define a new status for failed send because message file not found ?
        assertTrue(statusAppender.getAppendedText().contains(FILE_DOES_NOT_EXISTS));
    }


    @Test
    public void testMailStatusAlreadySentAddedToStatusAsAlreadySent() {
        final RunnerRequestMock request = new RunnerRequestMock(inputStrEMail1, statusStrEmail1AlreadySend, new MailSenderMock());
        new SequentialRunner(request).run();

        final AppenderMock statusAppender = (AppenderMock) request.getStatusAppender();
        assertTrue(statusAppender.getAppendedText().contains(Util.asCSVfield(OperationResult.AlreadySent)));
    }

}
