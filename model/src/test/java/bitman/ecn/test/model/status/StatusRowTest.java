package bitman.ecn.test.model.status;


import bitman.ecn.model.status.StatusField;
import bitman.ecn.model.status.StatusRow;
import bitman.ecn.model.status.StatusRowImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StatusRowTest
{
    @Test
    public void testConstructor()
    {
        assertNotNull(new StatusRowImpl(""));
    }

    @Test
    public void testEmptyRowHasOneField()
    {
        final StatusRow csvRow = new StatusRowImpl("");
        assertEquals(1, csvRow.fields());
    }

    @Test
    public void testEmptyRowHasOneEmptyField()
    {
        final StatusRow csvRow = new StatusRowImpl("");
        assertEquals("", csvRow.getField(0));
    }

    @Test
    public void testRowHasOneFields()
    {
        final StatusRow csvRow = new StatusRowImpl("field1");
        assertEquals(1, csvRow.fields());
    }

    @Test
    public void testRowHasTwoFields()
    {
        final StatusRow csvRow = new StatusRowImpl("field1,field2");
        assertEquals(2, csvRow.fields());
    }

    @Test
    public void testRowGetSecondField()
    {
        final StatusRow csvRow = new StatusRowImpl("field1,field2");
        assertEquals("field2", csvRow.getField(1));
    }

    @Test
    public void testRowGetEMAIL()
    {
        final StatusRow csvRow = new StatusRowImpl("field1,field2");
        assertEquals("field1", csvRow.getField(StatusField.EMAIL));
    }

    @Test
    public void testRowGetSTATUS()
    {
        final StatusRow csvRow = new StatusRowImpl("field1,field2");
        assertEquals("field2", csvRow.getField(StatusField.OPERATION_RESULT));
    }

    @Test
    public void testRowGetERRORMSG()
    {
        final StatusRow csvRow = new StatusRowImpl("field1,field2,ERR");
        assertEquals("ERR", csvRow.getField(StatusField.ERROR_MESSAGE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRowGetInvalidField()
    {
        final StatusRow csvRow = new StatusRowImpl("field1,field2");
        csvRow.getField(2);
    }

}
