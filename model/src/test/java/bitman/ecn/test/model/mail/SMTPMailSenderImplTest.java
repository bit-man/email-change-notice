package bitman.ecn.test.model.mail;

import bitman.ecn.model.mail.MailSendErrorException;
import bitman.ecn.model.mail.SMTPMailSenderImpl;
import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SMTPMailSenderImplTest
{

    private static final String MSG_BODY = "Hello, world";
    private static final String EMAIL_FROM = "user@example.com";
    private static final String EMAIL_TO = "foo@bar.com";
    private static final String MSG_SUBJECT = "TestMail";

    @Test
    public void testConstructor() {
        assertNotNull( new SMTPMailSenderImpl("localhost", 25, false) );
    }

    @Test
    public void testSMTPmsgSent()
            throws MailSendErrorException
    {
        SimpleSmtpServer server = null;
        try
        {
            server = SimpleSmtpServer.start(0);
            int port = server.getLocalPort();

            final SMTPMailSenderImpl smtpMailSender = new SMTPMailSenderImpl("localhost", port, false);
            BufferedReader msgStream = new BufferedReader( new StringReader(MSG_SUBJECT + "\n" + MSG_BODY));
            smtpMailSender.sendMessage(msgStream, EMAIL_FROM, EMAIL_TO);

            assertEquals(1, server.getReceivedEmailSize());

            final SmtpMessage msg = (SmtpMessage) server.getReceivedEmail().next();
            assertEquals(MSG_BODY, msg.getBody());
            assertEquals(EMAIL_FROM, msg.getHeaderValue("From"));
            assertEquals(EMAIL_TO, msg.getHeaderValue("To"));
            assertEquals(MSG_SUBJECT, msg.getHeaderValue("Subject"));

        }
        finally
        {
            if ( server != null && ! server.isStopped())
            {
                server.stop();
            }
        }
    }

    @Test
    public void testSMTPsendTwoMessagesAndBodiesAreOK()
            throws MailSendErrorException
    {
        SimpleSmtpServer server = null;
        try
        {
            server = SimpleSmtpServer.start(0);
            int port = server.getLocalPort();

            final SMTPMailSenderImpl smtpMailSender = new SMTPMailSenderImpl("localhost", port, SMTPMailSenderImpl.AVOID_SSL);

            BufferedReader msgStream = new BufferedReader( new StringReader(MSG_SUBJECT + "\n" + MSG_BODY));
            smtpMailSender.sendMessage(msgStream, EMAIL_FROM, EMAIL_TO);

            msgStream = new BufferedReader( new StringReader(MSG_SUBJECT + "\n" + MSG_BODY));
            smtpMailSender.sendMessage(msgStream, EMAIL_FROM, EMAIL_TO);

            assertEquals(2, server.getReceivedEmailSize());

            SmtpMessage msg = (SmtpMessage) server.getReceivedEmail().next();
            assertEquals(MSG_BODY, msg.getBody());

            msg = (SmtpMessage) server.getReceivedEmail().next();
            assertEquals(MSG_BODY, msg.getBody());


        }
        finally
        {
            if ( server != null && ! server.isStopped())
            {
                server.stop();
            }
        }
    }

    @Test(expected = MailSendErrorException.class )
    public void testSMTPmsgNotSentAnd()
            throws MailSendErrorException
    {
        SimpleSmtpServer server = null;
        try
        {
            server = SimpleSmtpServer.start(0);
            int port = server.getLocalPort();
            server.stop();

            final SMTPMailSenderImpl smtpMailSender = new SMTPMailSenderImpl("localhost", port, false);
            BufferedReader msgStream = new BufferedReader( new StringReader(MSG_BODY));
            smtpMailSender.sendMessage(msgStream, EMAIL_FROM, EMAIL_TO);
        }
        finally
        {
            if ( server != null && ! server.isStopped())
            {
                server.stop();
            }
        }
    }

}
