package bitman.ecn.test.model.exec;

import bitman.ecn.model.exec.RunnerRequest;
import bitman.ecn.model.exec.RunnerRequestBase;
import bitman.ecn.model.input.InputIteratorImpl;
import bitman.ecn.model.mail.MailSender;
import bitman.ecn.model.status.StatusIteratorImpl;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.StringReader;

public class RunnerRequestMock
        extends RunnerRequestBase
        implements RunnerRequest
{

    public RunnerRequestMock(@NotNull String inputStr, @NotNull String statusStr, @NotNull MailSender mailSender)
    {
        super(new StatusIteratorImpl(
                new BufferedReader(new StringReader(statusStr))
                ),
                mailSender,
                new InputIteratorImpl(
                    new BufferedReader(new StringReader(inputStr))
                ),
                new AppenderMock(),
                "someone@example.com");
    }

}
