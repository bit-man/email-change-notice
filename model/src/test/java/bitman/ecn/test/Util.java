package bitman.ecn.test;

import bitman.ecn.model.status.OperationResult;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class Util
{
    @NotNull
    public static File createTempFile()
            throws IOException
    {
        final File tempFile = File.createTempFile("test", "seq");
        tempFile.deleteOnExit();
        return tempFile;
    }


    @NotNull
    public static String asCSVfield(OperationResult operationResult)
    {
        return "," + operationResult.toString() + ",";
    }
}
