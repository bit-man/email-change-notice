package bitman.ecn.model.input;

import bitman.ecn.model.csv.CSVIterator;

import java.io.BufferedReader;

public class InputIteratorImpl
    extends CSVIterator<InputRow>
{
    public InputIteratorImpl(BufferedReader in)
    {
        super(in);
    }

    @Override
    protected InputRow createRow(String row)
    {
        return new InputRowImpl(row);
    }
}
