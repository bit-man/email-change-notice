package bitman.ecn.model.input;

import bitman.ecn.model.csv.CSVRowImpl;
import org.jetbrains.annotations.NotNull;

/**
 * CSV format for Email Change Notice
 */
public class InputRowImpl
    extends CSVRowImpl
    implements InputRow
{
    public InputRowImpl(@NotNull String row)
    {
        super(row);
    }


    public String getField(InputField field) {
        return getField(field.getPosition());
    }
}
