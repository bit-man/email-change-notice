package bitman.ecn.model.input;

/**
 * CSV fields for input file
 */
public enum InputField
{
    EMAIL(0), MESSAGE_ID(1);

    private final int position;

    InputField(int position)
    {
        this.position = position;
    }

    public int getPosition()
    {
        return position;
    }
}
