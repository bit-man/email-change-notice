package bitman.ecn.model.input;

import bitman.ecn.model.csv.CSVRow;

public interface InputRow
    extends CSVRow
{
    String getField(InputField field);
}
