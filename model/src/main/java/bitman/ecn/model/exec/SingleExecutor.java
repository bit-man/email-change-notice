package bitman.ecn.model.exec;

import bitman.ecn.model.csv.CSVIterator;
import bitman.ecn.model.input.InputField;
import bitman.ecn.model.input.InputRow;
import bitman.ecn.model.mail.MailSendErrorException;
import bitman.ecn.model.mail.MailSender;
import bitman.ecn.model.status.OperationResult;
import bitman.ecn.model.status.StatusField;
import bitman.ecn.model.status.StatusRow;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class SingleExecutor
        implements Executor
{
    private final InputRow input;
    private final CSVIterator<StatusRow> statusIterator;
    private final MailSender mailSender;
    private String from;

    public SingleExecutor(InputRow input, CSVIterator<StatusRow> status, MailSender mailSender,  @NotNull String from)
    {
        this.input = input;
        this.statusIterator = status;
        this.mailSender = mailSender;
        this.from = from;
    }

    private static BufferedReader createReader(String filename)
            throws FileNotFoundException
    {
        return new BufferedReader( new FileReader(filename)) ;
    }

    @Override
    public void send()
            throws EMailAlreadySendException, MailSendErrorException, FileNotFoundException
    {
        final String eMailTo = input.getField(InputField.EMAIL);
        OperationResult status = searchEmailLastStatus(eMailTo);

        switch (status)
        {
            case OK:
            case AlreadySent:
                throw new EMailAlreadySendException();

            case NotSent:
            case FailedSend:
                mailSender.sendMessage(createReader(input.getField(InputField.MESSAGE_ID)),
                        from, eMailTo);
                break;

        }

    }

    private OperationResult searchEmailLastStatus(String eMail)
    {
        OperationResult lastStatus = OperationResult.NotSent;

        while (statusIterator.hasNext())
        {
            final StatusRow statusRow = statusIterator.next();
            final String statusEMail = statusRow.getField(StatusField.EMAIL);
            if ((statusEMail).equals(eMail))
            {
                lastStatus = OperationResult.valueOf(statusRow.getField(StatusField.OPERATION_RESULT));
            }
        }
        return lastStatus;
    }

}
