package bitman.ecn.model.exec;

import bitman.ecn.model.csv.CSVIterator;
import bitman.ecn.model.input.InputField;
import bitman.ecn.model.input.InputRow;
import bitman.ecn.model.log.Logger;
import bitman.ecn.model.mail.MailSendErrorException;
import bitman.ecn.model.status.OperationResult;
import bitman.ecn.model.status.StatusRowImpl;
import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Writer;

/**
 * Description : Runs all e-mail executors sequentially
 */
public class SequentialRunner
        extends RunnerBase
        implements Runner
{

    public SequentialRunner(@NotNull RunnerRequest request)
    {
        super(request);
    }

    @Override
    public void run()
    {
        final CSVIterator<InputRow> iterator = request.getInputIterator();
        while (iterator.hasNext())
        {
            final InputRow row = iterator.next();
            final String email = row.getField(InputField.EMAIL);
            try
            {
                Logger.logInfo("Sending e-mail to " + request.getMailSender());
                new SingleExecutor(row, request.getStatusIterator(), request.getMailSender(), request.getEmailFrom()).send();
                Logger.logInfo("Sending OK ");
                writeStatus(email, request.getStatusAppender(), OperationResult.OK);
            } catch (FileNotFoundException|MailSendErrorException e)
            {
                Logger.logInfo("Sending Failed ");
                writeStatus(email, request.getStatusAppender(), OperationResult.FailedSend, e.getMessage() );
            } catch (EMailAlreadySendException e)
            {
                Logger.logInfo("Already send ");
                writeStatus(email, request.getStatusAppender(), OperationResult.AlreadySent);
            }
        }
    }

    private void writeStatus(String email, Writer statusAppender, OperationResult operationResult, String errorMsg)
    {

        final String status = StatusRowImpl.getStatusLine(email, operationResult, errorMsg);
        try
        {
            statusAppender.write(status);
            statusAppender.flush();
        } catch (IOException e1)
        {
            Logger.logErr(e1);
        }
    }

    private void writeStatus(String email, Writer statusAppender, OperationResult operationResult)
    {
        writeStatus(email,statusAppender,operationResult,"");
    }


}
