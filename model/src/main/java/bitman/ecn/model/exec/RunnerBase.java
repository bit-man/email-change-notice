package bitman.ecn.model.exec;

public abstract class RunnerBase
{

    protected final RunnerRequest request;

    public RunnerBase(RunnerRequest request)
    {
        this.request = request;
    }
}
