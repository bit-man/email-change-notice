package bitman.ecn.model.exec;

import bitman.ecn.model.csv.CSVIterator;
import bitman.ecn.model.input.InputIteratorImpl;
import bitman.ecn.model.input.InputRow;
import bitman.ecn.model.mail.MailSender;
import bitman.ecn.model.status.StatusIteratorImpl;
import bitman.ecn.model.status.StatusRow;

import java.io.Writer;

public class RunnerRequestBase
        implements RunnerRequest
{
    private CSVIterator<InputRow> inputIterator;
    private CSVIterator<StatusRow> statusIterator;
    private MailSender mailSender;
    private Writer statusAppender;
    private String emailFrom;

    public RunnerRequestBase(StatusIteratorImpl statusIterator, MailSender mailSender, InputIteratorImpl inputIterator, Writer statusAppender, String emailFrom)
    {
        this.inputIterator = inputIterator;
        this.statusIterator = statusIterator;
        this.mailSender = mailSender;
        this.statusAppender = statusAppender;
        this.emailFrom = emailFrom;
    }


    public CSVIterator<InputRow> getInputIterator()
    {
        return inputIterator;
    }

    public MailSender getMailSender()
    {
        return mailSender;
    }

    public CSVIterator<StatusRow> getStatusIterator()
    {
        return statusIterator;
    }

    public Writer getStatusAppender()
    {
        return statusAppender;
    }

    @Override
    public String getEmailFrom()
    {
        return emailFrom;
    }
}
