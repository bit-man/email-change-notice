package bitman.ecn.model.exec;


import bitman.ecn.model.input.InputIteratorImpl;
import bitman.ecn.model.mail.MailSender;
import bitman.ecn.model.status.StatusIteratorImpl;
import org.jetbrains.annotations.NotNull;

import java.io.*;

public class RunnerRequestFile
        extends RunnerRequestBase
        implements RunnerRequest
{
    public RunnerRequestFile(File inputFile, File statusfile, MailSender mailSender, @NotNull String emailFrom)
            throws IOException
    {
        super(
                new StatusIteratorImpl(
                        new BufferedReader(new FileReader(statusfile))
                ),
                mailSender,
                new InputIteratorImpl(
                        new BufferedReader(new FileReader(inputFile))
                ),
                new BufferedWriter(
                        new FileWriter(statusfile, true)
                ),
                emailFrom);
    }

}
