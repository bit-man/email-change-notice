package bitman.ecn.model.exec;

import bitman.ecn.model.csv.CSVIterator;
import bitman.ecn.model.input.InputRow;
import bitman.ecn.model.mail.MailSender;
import bitman.ecn.model.status.StatusRow;

import java.io.Writer;

public interface RunnerRequest
{
    CSVIterator<InputRow> getInputIterator();

    MailSender getMailSender();

    CSVIterator<StatusRow> getStatusIterator();

    Writer getStatusAppender();

    String getEmailFrom();
}
