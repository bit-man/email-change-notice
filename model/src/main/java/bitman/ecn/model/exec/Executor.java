package bitman.ecn.model.exec;

import bitman.ecn.model.mail.MailSendErrorException;

import java.io.FileNotFoundException;

/**
 * Executes the basic sending and status logging
 */
public interface Executor
{
    void send()
            throws EMailAlreadySendException, MailSendErrorException, FileNotFoundException;
}
