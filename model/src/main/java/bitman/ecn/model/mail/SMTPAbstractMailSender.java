package bitman.ecn.model.mail;

public abstract class SMTPAbstractMailSender
    implements SMTPMailSender
{
    protected int port;
    protected String host;

    @Override
    public void setPort(int port)
    {
        this.port = port;
    }

    @Override
    public void setHost(String host)
    {
        this.host = host;
    }

}
