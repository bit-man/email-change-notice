package bitman.ecn.model.mail;

public interface SMTPMailSender
        extends MailSender
{
    void setPort(int port);

    void setHost(String host);
}
