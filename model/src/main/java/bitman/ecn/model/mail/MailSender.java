package bitman.ecn.model.mail;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;

public interface MailSender
{

    void setCredentials(String user, String password);
    /**
     * Send e-mail
     * @param in  first line contains e-mail subject, the remaining lines contain the body
     * @param from sender's e-mail
     * @param to  destination e-mail
     * @throws MailSendErrorException if any error occurs while e-mail sending
     */
    void sendMessage(BufferedReader in, String from,@NotNull String to)
            throws MailSendErrorException;
}
