package bitman.ecn.model.mail;

import java.io.BufferedReader;

/**
 * E-mail sending
 */

public interface Mailer
{
    void sendMessage(BufferedReader in, String from, String emailTo, String subject)
            throws MailSendErrorException;
}
