package bitman.ecn.model.mail;

import bitman.ecn.model.log.Logger;
import org.jetbrains.annotations.NotNull;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Properties;

public class SMTPMailSenderImpl implements MailSender
{
    private final String host;
    private final int port;
    private boolean isSSL;
    private String password = null;
    private String username = null;

    public static boolean USE_SSL = true;
    public static boolean AVOID_SSL = ! USE_SSL;

    public SMTPMailSenderImpl(String host, int port, boolean isSSL)
    {
        this.host = host;
        this.port = port;
        this.isSSL = isSSL;
    }

    public void setCredentials(@NotNull String user, @NotNull String password)
    {
        this.username = user;
        this.password = password;
        isSSL = true;
    }

    @Override
    public void sendMessage(BufferedReader in, String emailFrom, @NotNull String emailTo)
            throws MailSendErrorException
    {
        Transport transport = null;
        try
        {
            /**
             * Take a look at http://stackoverflow.com/questions/26548059/sending-email-with-ssl-using-javax-mail
             * and http://www.rgagnon.com/javadetails/java-0570.html for pointers on how to make it work
             */
            Properties props = new Properties();

            props.put("mail.debug", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.socketFactory.port", port);
            props.put("mail.smtp.socketFactory.class", isSSL ?
                                                "javax.net.ssl.SSLSocketFactory" :
                                                "javax.net.ServerSocketFactory");
            props.put("mail.smtp.auth", isAuthenticationEnabled());
            props.put("mail.smtp.port", port);
            props.put("mail.transport.protocol", isSSL ? "smtps" : "smtp");

            Session session = Session.getDefaultInstance(props);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailFrom));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));

            final ParseReader parseReader = new ParseReader(in).invoke();
            message.setSubject(parseReader.getSubject());
            message.setText(parseReader.getMsg());

            transport = session.getTransport();
            connect(transport);
            transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));

        } catch (Throwable e)
        {
            Logger.logErr(e);
            throw new MailSendErrorException(e.getMessage());
        }
        finally
        {
            if (transport != null)
            {
                try
                {
                    transport.close();
                } catch (MessagingException e)
                {
                    Logger.logErr(e);
                }
            }
        }

    }

    private void connect(Transport transport)
            throws MessagingException
    {
        if ( isAuthenticationEnabled())
        {
            transport.connect(host, port, username, password);
        } else {
            transport.connect(host, String.valueOf(port));
        }
    }

    private boolean isAuthenticationEnabled()
    {
        return this.username != null && this.password != null;
    }

    private class ParseReader
    {
        private BufferedReader in;
        private String subject;
        private String msg;

        public ParseReader(BufferedReader in)
        {
            this.in = in;
        }

        public String getSubject()
        {
            return subject;
        }

        public String getMsg()
        {
            return msg;
        }

        public ParseReader invoke()
                throws IOException
        {
            subject = extractSubject();
            msg = extractBody();
            return this;
        }

        private String extractSubject()
                throws IOException
        {
            return in.readLine();
        }

        private String extractBody()
                throws IOException
        {
            String msg = "";
            String line;
            while( ( line =  in.readLine() ) != null) {
                msg += line + "\n";
            }
            return msg;
        }
    }
}
