package bitman.ecn.model.mail;

import java.io.BufferedReader;

/**
 * E-mail sending using SMTP protocol
 */

public abstract class AbstractMailer
        implements Mailer
{

    protected final MailSender sender;

    public AbstractMailer( MailSender sender) {
        this.sender = sender;
    }

    @Override
    public void sendMessage(BufferedReader in, String emailFrom, String emailTo, String subject)
            throws MailSendErrorException
    {
        sender.sendMessage(in, emailFrom, emailTo);
    }
}
