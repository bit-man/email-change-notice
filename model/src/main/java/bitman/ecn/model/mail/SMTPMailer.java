package bitman.ecn.model.mail;

import java.io.BufferedReader;

public class SMTPMailer
    extends AbstractMailer
{

    private int port;
    private String host;

    public SMTPMailer(String host, int port, SMTPMailSender sender)
    {
        super(sender);
        this.host = host;
        this.port = port;
    }


    @Override
    public void sendMessage(BufferedReader in, String emailFrom, String emailTo, String subject)
            throws MailSendErrorException
    {
        ((SMTPMailSender) sender).setHost(host);
        ((SMTPMailSender) sender).setPort(port);
        super.sendMessage(in, emailFrom, emailTo, subject);
    }
}
