package bitman.ecn.model.mail;

public class MailSendErrorException
        extends Throwable
{
    public MailSendErrorException(String message)
    {
        super(message);
    }
}
