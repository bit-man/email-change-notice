package bitman.ecn.model.log;

public class Logger
{
    private static final String INFO = "[INFO] ";
    private static final String ERROR = "[ERROR] ";

    private static String HEADER = "ecn: ";

    public static void logInfo(String msg) {
        log(INFO + msg);
    }

    public static void logErr(String msg) {
        log(ERROR + msg);
    }

    public static void logErr(Throwable e) {
        log(ERROR + e.getMessage());
        e.printStackTrace(System.out);
    }

    private static void log(String msg) {
        System.out.println(HEADER + msg + "\n");
    }
}
