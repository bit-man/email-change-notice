package bitman.ecn.model.status;

import bitman.ecn.model.csv.CSVIterator;

import java.io.BufferedReader;

/**
 * Description : Status iterator
 */
public class StatusIteratorImpl
        extends CSVIterator<StatusRow>
{
    public StatusIteratorImpl(BufferedReader in)
    {
        super(in);
    }

    @Override
    protected StatusRow createRow(String row)
    {
        return new StatusRowImpl(row);
    }
}
