package bitman.ecn.model.status;

import java.util.Iterator;

public interface StatusIterator
    extends Iterator<StatusRow>
{
}
