package bitman.ecn.model.status;

import bitman.ecn.model.csv.CSVRow;

public interface StatusRow
    extends CSVRow
{
    String getField(StatusField field);
}
