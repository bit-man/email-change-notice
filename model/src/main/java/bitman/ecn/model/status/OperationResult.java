package bitman.ecn.model.status;

public enum OperationResult
{
    OK,             // e-mail was sent
    NotSent,        // e-mail not sent (not saved in status file) ToDo redefine, use only operation results available at status file
    AlreadySent,    // e-mail was sent already, not resend
    FailedSend      // e-mail send attempted but failed
}
