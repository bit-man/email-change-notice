package bitman.ecn.model.status;

import bitman.ecn.model.csv.CSVRowImpl;
import org.jetbrains.annotations.NotNull;

/**
 * CSV format for Email Change Notice
 */
public class StatusRowImpl
    extends CSVRowImpl
    implements StatusRow
{
    public StatusRowImpl(@NotNull String row)
    {
        super(row);
    }

    @NotNull
    public static String getStatusLine(String email, OperationResult operationResult)
    {
        return getStatusLine(email,operationResult,"");
    }

    @NotNull
    public static String getStatusLine(String email, OperationResult operationResult, String errMsg)
    {
        return email + "," + operationResult + "," + errMsg + "\n";
    }


    public String getField(StatusField field) {
        return getField(field.getPosition());
    }
}
