package bitman.ecn.model.status;

/**
 * Status file fields
 */
public enum StatusField
{
    EMAIL(0), OPERATION_RESULT(1), ERROR_MESSAGE(2);

    private final int position;

    StatusField(int position)
    {
        this.position = position;
    }

    public int getPosition()
    {
        return position;
    }
}
