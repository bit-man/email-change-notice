package bitman.ecn.model.csv;


import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class CSVRowImpl
        implements CSVRow
{
    private final String[] row;

    public CSVRowImpl(@NotNull String row)
    {
        this.row = row.split(",");
    }

    public int fields()
    {
        return row.length;
    }

    public String getField(int i)
    {
        if (i > row.length - 1)
        {
            throw new IllegalArgumentException();
        }
        return row[i];
    }

    public boolean equals(Object o) {
        if ( o == this) {
            return true;
        }

        if ( ! (o instanceof CSVRowImpl)) {
            return false;
        }

        final CSVRowImpl that = (CSVRowImpl) o;
        return ( this.toString().equals( that.toString() ) );
    }

    @Override
    public String toString()
    {
        return "CSVRowImpl{" +
                Arrays.toString(row) +
                '}';
    }
}
