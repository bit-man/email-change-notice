package bitman.ecn.model.csv;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;

public class CSVIteratorImpl
        extends CSVIterator<CSVRow>
{
    public CSVIteratorImpl(@NotNull BufferedReader in)
    {
        super(in);
    }

    @Override
    protected CSVRow createRow(String row)
    {
        return new CSVRowImpl(row);
    }
}
