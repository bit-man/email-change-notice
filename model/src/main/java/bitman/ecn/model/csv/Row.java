package bitman.ecn.model.csv;


public interface Row<T>
{
    int fields();

    T getField(int i);
}
