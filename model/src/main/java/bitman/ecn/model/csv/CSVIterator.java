package bitman.ecn.model.csv;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;

public abstract class CSVIterator<T>
        implements Iterator<T>
{
    private final BufferedReader in;
    private boolean init;
    private boolean hasNext;
    private T nextRow;

    public CSVIterator(BufferedReader in)
    {
        this.in = in;
        hasNext = false;
        nextRow = null;
        init = false;
        init();
    }

    @Override
    public boolean hasNext()
    {
        return hasNext;
    }

    private void init()
    {
        if (!init)
        {
            init = true;
            doNext();
        }

    }

    protected abstract T createRow(String row);

    private void doNext()
    {
        try
        {
            final String row = in.readLine();
            if (row != null)
            {
                nextRow = createRow(row);
                hasNext = true;
            } else
            {
                hasNext = false;
            }
        } catch (IOException e)
        {
            hasNext = true;
        }
    }

    @Override
    public T next()
    {
        T retrunRow = nextRow;
        doNext();
        return retrunRow;
    }

    @Override
    public void remove()
    {
        throw new IllegalArgumentException("remove() operation not implemented");
    }
}
