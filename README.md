# email-change-notice
Will change your e-mail and need to tell to your friends, family and other people in the galaxy ?
Too busy for sending each of them a few lines about the e-mail change ?

Let email-change-notice do *some* the work for you !!

Should I put emphasis in the word **some** ? X-D

## Requirements

These are the initial requirement for this tiny work of me. The real details about how to
use it should be found after these requirements

* e-mails to be contacted must be fed from a file (CSV preferred)
* simple custom messages (not fancy scripting or variable replacements)
* let it have execution status, meaning if some e-mail send fails when the program is
executed again make it send only the failed ones, new ones added to the list, etc.
* because a lot of e-mail will be send avoid e-mail system mark the sender account as spammer
* GUI should be nice, but not a must

## Usage

The CLI interface is the only one available. The class to invoke is bitman.ecn.cli.Main
with 5 parameters :

* input file path : path to input file containing destination address information
* status file path : path to status file where e-mail sending information is stored
* SMTP host : SMTP host address
* SMTP port : STMP port
* sender's e-mail  : your e-mail

### input file format

This file contains the information to whom the e-mail will be sent and the messages to deliver.
It is a CSV (comma separated value) formatted containing one line per edstination address
with the next values :

* e-mail : destination e-mail
* message file path : path to a file containing the e-mail subject in the first line and the message body
(plain text) in the remaining ones.

