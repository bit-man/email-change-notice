package bitman.ecn.cli;


import bitman.ecn.model.exec.RunnerRequestFile;
import bitman.ecn.model.exec.SequentialRunner;
import bitman.ecn.model.log.Logger;
import bitman.ecn.model.mail.SMTPMailSenderImpl;

import java.io.File;
import java.io.IOException;

public class Main
{
    public static void main(String[] arg)
    {

        try
        {
            ParamsValidator validator = new ParamsValidator(arg).invoke();
            File inputFile = validator.getInputFile();
            File statusFile = validator.getStatusFile();
            String host = validator.getHost();
            int port = validator.getPort();
            String emailFrom = validator.getEmailFrom();

            SMTPMailSenderImpl smtpMailSender = new SMTPMailSenderImpl(host, port, SMTPMailSenderImpl.USE_SSL);

            if ( validator.isHasCredentials()) {
                smtpMailSender.setCredentials(validator.getUser(), validator.getPassword());
            }

            // ToDo create generic RunnerRequest with different constructors instead
            new SequentialRunner(new RunnerRequestFile(inputFile, statusFile, smtpMailSender, emailFrom)).run();
        } catch (IllegalArgumentException e)
        {
            Logger.logErr(e.getLocalizedMessage());
            showHelp();
            System.exit(-1);
        } catch (Throwable e)
        {
            Logger.logErr( e.getLocalizedMessage());
            System.exit(-1);
        }
    }

    private static void showHelp()
    {
        System.out.println("\n" +
                "usage :\n" +
                "\t" + Main.class.getName() + " inputFilePath statusfilePath SMTPhost SMTPport sender's_e-mail\n" +
                "\t\t[user password]" +
                "\n");
    }

    private static class ParamsValidator
    {
        private String[] arg;
        private File inputFile;
        private File statusFile;
        private String host;
        private int port;
        private String emailFrom;
        private String user;
        private String password;
        private boolean hasCredentials = false;

        public ParamsValidator(String... arg)
        {
            this.arg = arg;
        }

        public File getInputFile()
        {
            return inputFile;
        }

        public File getStatusFile()
        {
            return statusFile;
        }

        public String getHost()
        {
            return host;
        }

        public int getPort()
        {
            return port;
        }

        public String getEmailFrom()
        {
            return emailFrom;
        }

        public String getUser()
        {
            return user;
        }

        public String getPassword()
        {
            return password;
        }

        public boolean isHasCredentials()
        {
            return hasCredentials;
        }


        public ParamsValidator invoke()
                throws IOException
        {
            if (arg.length < 5)
            {
                throw new IllegalArgumentException("Missing parameters");
            }

            inputFile = new File(arg[0]);
            statusFile = new File(arg[1]);
            host = arg[2];
            port = Integer.parseInt(arg[3]);
            emailFrom = arg[4];


            if (arg.length == 7) {
                this.hasCredentials = true;
                this.user = arg[5];
                this.password = arg[6];
            }

            if (!inputFile.exists())
            {
                throw new IllegalArgumentException("File " + inputFile.getAbsolutePath() + " does not exist");
            }

            if (!statusFile.exists())
            {
                statusFile.createNewFile();
                Logger.logInfo("Status file created");
            }

            return this;
        }
    }
}
